
import React from 'react';
import { Link, Navigate, Route,BrowserRouter as Router, Routes } from 'react-router-dom';
import { Suspense } from 'react/cjs/react.production.min';
import './App.scss';
import Banner from './components/Banner';
import Header from './components/Header';
import NotFound from './components/NotFound';
import AddEdit from './features/Photo/pages/AddEdit';
import Main from './features/Photo/pages/Main';

const Photo = React.lazy(()=>import('./features/Photo'))

function App() {
  return (
    <Suspense fallback={<div>Loading....</div>}>
        <Router>
            <Header />
            <Routes>
                <Route path="photos" element={<Photo />}>
                    <Route path="add" element={<AddEdit />}/>
                    <Route path=":photoTitle" element={<AddEdit />}/>
                    <Route index element={<Main />}/>
                </Route>
                <Route path="/" element={<Navigate to="/photos" />}/>
            </Routes>
        </Router>
    </Suspense>
  );
}

export default App;
