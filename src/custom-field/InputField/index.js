import React from 'react';
import { FormGroup, Label , Input } from 'reactstrap';

function InputField(props) {
    const {field,form,type,label,placeholder,disabled} = props;
    const {name} = field;
    console.log(field)
    return (
        <FormGroup>
            {label && <Label for={name}>{label}</Label>}
            <Input 
            id={name} 
            {...field}
            type={type}
            disabled={disabled}
            placeholder={placeholder}/>
        </FormGroup>
    );
}

export default InputField;