import React from 'react';
import { FormGroup, Label } from 'reactstrap';
import RandomPhoto from '../../components/RandomPhoto';

function RandomPhotoField(props) {
    const {field, form, label} = props;
    const {name, value, onBlur} = field;
    const handleImageUrlChange = (newImageUrl)=>{
        form.setFieldValue(name,newImageUrl)
    }
    return (
        <FormGroup>
            {label && <Label for={name}>{label}</Label>}
            <RandomPhoto 
            name={name}
            imageUrl = {value}
            onImageUrlChange = {handleImageUrlChange}
            onRandombuttonBlur ={onBlur}
            />
            
        </FormGroup>
    );
}

export default RandomPhotoField;