import React from 'react';
import Select from 'react-select';
import { FormGroup, Input, Label } from 'reactstrap';

function SelectField(props) {
    const {field,form,options,label,placeholder,disabled} = props;
    const {name,value} = field;
    const selectOptions = options.find(option => option.value === value)
    console.log(field);

    const handleSelectedOptionChange = (selectedOption)=>{
        const selectedValue = selectedOption?selectedOption.value:selectedOption
        const changeEvent = {
            target:{
                name:name,
                value:selectedValue
            }
        }
        field.onChange(changeEvent)
    }
    return (
        <FormGroup>
            {label && <Label for={name}>{label}</Label>}
            <Select 
            id={name} 
            {...field}
            value={selectOptions}
            onChange={handleSelectedOptionChange}
            disabled={disabled}
            placeholder={placeholder}
            options={options}
            />
        </FormGroup>
    );
}

export default SelectField;