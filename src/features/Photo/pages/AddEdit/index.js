import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams  } from 'react-router-dom';
import Banner from '../../../../components/Banner';
import Images from '../../../../constant/image';
import PhotoForm from '../../components/PhotoForm';
import { addPhoto ,editPhoto } from '../../photoSlice';

function AddEdit() {
    const dispatch = useDispatch();
    const navigate  = useNavigate ();
    const {photoTitle} = useParams();
    const isAddMode = !photoTitle;

    const editedPhoto = useSelector(state => state.photos.find(photo =>photo.title===photoTitle))
    const initialValues = isAddMode ? {
        title:'',
        categoryId:null,
        photo:'',
    }:editedPhoto

    const handleOnSubmit = (value) => {
        return new Promise((resolve) => {
            setTimeout(() =>{
                if(isAddMode){
                    dispatch(addPhoto(value))
                }else{
                    dispatch(editPhoto(value))
                }
                
                navigate('/photos')
                resolve(true)
            },2000)
        })
    }
    return (
        <div>
            <Banner title="Your awesome Photo" backgroundUrl = {Images.THIRD_BG}/>
            <PhotoForm 
            isAddMode = {isAddMode}
            initialValues={initialValues}
            onSubmit={handleOnSubmit}
            />
        </div>
    );
}

export default AddEdit;