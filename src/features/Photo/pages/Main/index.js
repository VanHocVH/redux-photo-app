import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useNavigate} from 'react-router-dom';
import { Container } from 'reactstrap';
import Banner from '../../../../components/Banner';
import PhotoList from '../../../../components/PhotoList';
import Images from '../../../../constant/image';
import { removePhoto } from '../../photoSlice';

function Main(props) {
    const photos  = useSelector(state => state.photos)
    const dispatch = useDispatch()
    const navigate  = useNavigate ();
    console.log(photos)
    const handlePhotoEditClick = (photos)=>{
        console.log("Edit", photos);
        const urlEdit = `/photos/${photos.title}`
        navigate(urlEdit)
    }
    const handlePhotoRemoveClick = (photos)=>{
        console.log("Remove", photos);
        const action = removePhoto(photos)
        dispatch(action)
    }
    return (
        <div>

            <Banner title="Your awesome Photo" backgroundUrl = {Images.SECOND_BG}/>
            <Container className="photo">
                <Link to="/photos/add"> Add new photo</Link>
                <PhotoList 
                photoList={photos}
                onPhotoEditClick={handlePhotoEditClick}
                onPhotoRemoveClick={handlePhotoRemoveClick}
                />
            </Container>
        </div>
    );
}

export default Main;