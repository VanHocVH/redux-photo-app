import { createSlice } from "@reduxjs/toolkit";

const photo = createSlice({
    name:'photo',
    initialState: [],
    reducers:{
        addPhoto: ( state, action) =>{
            state.push(action.payload)
        },
        removePhoto: (state,action) =>{
            console.log(action)
            state = state.filter(item=> item.title !== action.payload.title&& item.photo !== action.payload.photo)
            return state
        },
        editPhoto: (state,action) =>{
            const newsPhoto = action.payload
            const editPhoto = state.findIndex((item)=>item.title === action.payload.title)   
            const oldPhoto = state[editPhoto] 
            console.log(oldPhoto,newsPhoto);
            if(editPhoto>=0){
                state[editPhoto] =newsPhoto
            }
        }
    },
})

const { reducer, actions } = photo
export const {addPhoto,removePhoto,editPhoto} = actions
export default reducer