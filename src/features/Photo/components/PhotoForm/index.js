import { Formik, Form, FastField } from 'formik';
import React from 'react';
import Select from 'react-select';
import { Button, FormGroup, Input, Label, Spinner} from 'reactstrap';
import { PHOTO_CATEGORY_OPTIONS } from '../../../../constant/global';
import Images from '../../../../constant/image';
import InputField from '../../../../custom-field/InputField';
import RandomPhotoField from '../../../../custom-field/RandomPhotoField';
import SelectField from '../../../../custom-field/SelectField';

function PhotoForm(props) {
    const {initialValues ,isAddMode} = props
    return (
        <Formik
        initialValues = {initialValues}
        onSubmit={props.onSubmit}
        >
            {formikProps=>{
                const {values,errors,touched, isSubmitting} = formikProps
                return(

                    <Form >
                        <FastField 
                        name="title"
                        component={InputField}

                        label="Title"
                        placeholder="Eg: Wow nature ...."
                        />

                        <FastField 
                        name="categoryId"
                        component={SelectField}

                        label="Category"
                        placeholder="What's your photo category"
                        options={PHOTO_CATEGORY_OPTIONS}
                        />

                        <FastField 
                        name="photo"
                        component={RandomPhotoField}

                        label="Photo"
                        />
                        <FormGroup>
                            <Button type="submit" color={isAddMode?'primary':'danger'}>
                                {isSubmitting && <Spinner size="md"/>}
                                {isAddMode? 'Add to album':'Edit this photo'}
                            </Button>
                        </FormGroup>
                    </Form>
                )
            }}
        </Formik>
    );
}

export default PhotoForm;