import React from 'react';
import { Outlet, Route, Routes, useLocation } from 'react-router-dom';
import AddEdit from './pages/AddEdit';
import Main from './pages/Main';

function Photo(props) {
    const match = useLocation();
    console.log(match);
    return (
        <div>
            <div className="content">
                <Outlet />
            </div>
        </div>
    );
}

export default Photo;