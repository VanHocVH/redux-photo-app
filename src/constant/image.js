import firstImage from '../assets/image/Anh-thien-nhien-1.jpg'
import secondImage from '../assets/image/Anh-thien-nhien-4.jpg'
import thirdImage from '../assets/image/anh-thien-nhien-dep-3.jpeg'
import fourImage from '../assets/image/anh-dep-nhat-the-gioi-ve-thien-nhien_041753462.jpg'

const Images = {
    FIRST_BG : firstImage,
    SECOND_BG : secondImage,
    third_BG : thirdImage,
    FOUR_BG : fourImage,
}
export default Images