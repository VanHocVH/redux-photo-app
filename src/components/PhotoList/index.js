import React from 'react';

function PhotoList(props) {

    const handleEdit = (photo)=>{
        props.onPhotoEditClick(photo);
    }
    const handleRemove = (photo)=>{
        props.onPhotoRemoveClick(photo);
    }

    return (
        <div>
            {props.photoList.map((item,index)=>{
                return(
                    <div key={index}>
                        <img src={item.photo}/>
                        <div>
                            <button onClick={()=>{handleEdit(item)}}>edit</button>
                            <button onClick={()=>{handleRemove(item)}}>remove</button>
                        </div>
                    </div>
                )
            })}
        </div>
    );
}

export default PhotoList;