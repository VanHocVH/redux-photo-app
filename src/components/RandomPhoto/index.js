import React from 'react';
import {Button} from 'reactstrap'

const getRandomImageUrl = ()=>{
    const randomId = Math.trunc(Math.random()*2000)
    return `https://picsum.photos/id/${randomId}/200/300`
}
function RandomPhoto({name,imageUrl,onImageUrlChange,onRandombuttonBlur}) {

    const handlerandomPhotoClick = ()=>{
        if(onImageUrlChange){
            onImageUrlChange(getRandomImageUrl())
        }
    }

    return (
        <div>
            <div>
                <Button 
                    name={name}
                    outline 
                    color="primary" 
                    onBlur={onRandombuttonBlur}
                    onClick={handlerandomPhotoClick}
                >
                    Random a photo
                </Button>

            </div>
            <div>
                {imageUrl&&<img width="200" height="200" 
                src={imageUrl} 
                alt="Opp... image not found. Please click random"
                onError={handlerandomPhotoClick}
                />}
            </div>
        </div>
    );
}

export default RandomPhoto;